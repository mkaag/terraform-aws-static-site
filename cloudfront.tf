resource "aws_cloudfront_origin_access_identity" "custom_oai" {
  comment = "access-identity-${var.name_prefix}.s3.amazonaws.com"
}

resource "aws_cloudfront_distribution" "cdn_website" {
  enabled             = true
  is_ipv6_enabled     = true
  http_version        = "http2"
  default_root_object = var.index_document
  aliases             = var.aliases
  price_class         = "PriceClass_100"        // USA, Canada, Europe

  origin {
    domain_name = aws_s3_bucket.s3.bucket_regional_domain_name
    origin_id   = "access-identity-${var.name_prefix}.s3.amazonaws.com"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.custom_oai.cloudfront_access_identity_path
    }
  }

  logging_config {
    include_cookies = false
    bucket          = var.log_bucket
    prefix          = "${var.domain}/"
  }

  custom_error_response {
    error_caching_min_ttl = 3600
    error_code            = 403
    response_code         = 403
    response_page_path    = "/${var.error_document}"
  }

  custom_error_response {
    error_caching_min_ttl = 3600
    error_code            = 404
    response_code         = 404
    response_page_path    = "/${var.error_document}"
  }

  default_cache_behavior {
    target_origin_id       = "access-identity-${var.name_prefix}.s3.amazonaws.com"
    viewer_protocol_policy = "redirect-to-https"
    allowed_methods        = ["GET", "HEAD","OPTIONS"]
    cached_methods         = ["GET", "HEAD"]
    min_ttl                = 86400
    default_ttl            = 86400
    max_ttl                = 2628000
    smooth_streaming       = false
    compress               = true

    // https://github.com/hashicorp/terraform/issues/10938
    // lambda_function_association = {}

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  viewer_certificate {
    acm_certificate_arn      = var.acm_certificate_arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.2_2019"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

   tags = merge(
    { "name" = var.name_prefix },
    var.tags
  )
}

resource "aws_s3_bucket_policy" "s3" {
  bucket = aws_s3_bucket.s3.id
  policy = data.aws_iam_policy_document.s3.json
}

data "aws_iam_policy_document" "s3" {
  statement {
    sid    = "DenyUnencrypted"
    effect = "Deny"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    actions = [
      "s3:PutObject",
    ]

    condition {
      test     = "StringNotEquals"
      variable = "s3:x-amz-server-side-encryption"
      values   = ["AES256"]
    }

    resources = [
      "${aws_s3_bucket.s3.arn}/*",
    ]
  }

  statement {
    sid    = "AllowCloudFrontList"
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.custom_oai.iam_arn]
    }

   actions   = ["s3:ListBucket"]
   resources = [aws_s3_bucket.s3.arn]
  }

  statement {
    sid    = "AllowCloudFrontGet"
    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.custom_oai.iam_arn]
    }

    actions = ["s3:GetObject"]

    resources = ["${aws_s3_bucket.s3.arn}/*"]
  }
}

