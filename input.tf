variable "name_prefix" {
  description = "A prefix to prepend to all 'Name' tags"
  type        = string
}

variable "dns_names_to_zone_names" {
  description = "A map of FQDNs to their Route53 Zone names that this site should be accessible at. Ex: ['example.com' => 'example.com', 'foo.example.com' => 'foo.example.com', '*.bar.example.com' => 'bar.example.com']"
  type        = map(string)
}

variable "acm_certificate_arn" {
  description = "The ARN of the certificate to use for this domain"
  type        = string
}

variable "log_bucket" {
  description = "The S3 bucket to write logs to"
  type        = string
}

# variable "healthcheck_sns_arn" {
#   description = "The ARN of the SNS topic to notify when the configured healthcheck is unhealthy"
#   type        = string
# }

variable "domain" {
  description = "The domain to distribute"
  type        = string
}

variable "aws_region" {
  description = "The AWS Region where the bucket must be located"
  type        = string
  default     = "eu-central-1"
}

variable "index_document" {
  description = "The HTML document used as index"
  type        = string
  default     = "index.html"
}

variable "error_document" {
  description = "The HTML document used as error"
  type        = string
}

variable "tags" {
  description = "A map of tags to apply to all resources"
  type        = map(string)
  default     = { "managedWith": "Terraform" }
}

variable "aliases" {
  description = "A list of domain aliases"
  type        = list(string) 
}

variable "cors_allowed_headers" {
  description = "List of headers allowed in CORS"
  type 	      = list(string)
  default     = []
}

variable "cors_allowed_methods" {
  description = "List of methods allowed in CORS"
  type        = list(string)
  default     = ["GET"]
}

variable "cors_allowed_origins" {
  description = "List of origins allowed to make CORS requests"
  type        = list(string)
  default     = ["https://s3.amazonaws.com"]
}

variable "cors_expose_headers" {
  description = "List of headers to expose in CORS response"
  type        = list(string)
  default     = []
}

variable "cors_max_age_seconds" {
  description = "Specifies time in seconds that browser can cache the response for a preflight request"
  type        = string
  default     = 3000
}
