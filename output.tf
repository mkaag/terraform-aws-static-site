output "bucket_arn" {
  description = "The name of the s3 bucket that the distribution fronts"
  value       = aws_s3_bucket.s3.arn
}

output "bucket_name" {
  description = "The name of the s3 bucket that the distribution fronts"
  value       = aws_s3_bucket.s3.id
}

output "bucket_website_endpoint" {
  description = "The website endpoint, if the bucket is configured with a website. If not, this will be an empty string."
  value       = aws_s3_bucket.s3.website_endpoint
}

output "deployer_access_key_id" {
  description = "The AWS access key id that can deploy to the site"
  value       = aws_iam_access_key.deployer.id
}

output "deployer_secret_access_key" {
  description = "The AWS secret access key that can deploy to the site"
  value       = aws_iam_access_key.deployer.secret
}

output "domain_name" {
  description = "Fully qualified domain name of the CDN"
  value       = aws_cloudfront_distribution.cdn_website.domain_name
}

output "zone_id" {
  description = "Zone where the distribution is hosted"
  value       = aws_cloudfront_distribution.cdn_website.hosted_zone_id
}

output "distribution_id" {
  description = "The ID of the created cloudfront distribution"
  value       = aws_cloudfront_distribution.cdn_website.id
}


