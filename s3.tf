resource "aws_s3_bucket" "s3" {
  bucket_prefix = "${var.name_prefix}-"
  acl           = "private"
  force_destroy = true

  website {
    index_document = var.index_document
    error_document = var.error_document
  }

  cors_rule {
    allowed_headers = var.cors_allowed_headers
    allowed_methods = var.cors_allowed_methods
    allowed_origins = var.cors_allowed_origins
    expose_headers  = var.cors_expose_headers
    max_age_seconds = var.cors_max_age_seconds
  }

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags = merge(
    { "name" = var.name_prefix },
    var.tags
  )
}

